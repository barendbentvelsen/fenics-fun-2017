import os
import random
# Addepted from : https://ubuntuforums.org/showthread.php?t=1058395&page=3


def main():

    if os.path.isfile('fun_insults.txt'):
        insults = 'yes'
        file = open('fun_insults.txt', 'r')
        list = file.readlines()
    else:
        insults = "no"

    print("Welcome to guess the number\n===========================")
    print("\nI'm thinking of a number, you have to guess what it is.\n")

    num = random.randrange(20)
    guess = ""

    while guess != num:
        guess = int(input("Take a guess: "))
        if guess < num:
            if insults == "yes":
                print(random.choice(list))
            print("Guess higher next time\n")
        elif guess > num:
            if insults == "yes":
                print(random.choice(list))
            print("Guess lower next time\n")
    print("!!***CONGRATULATIONS***!!")
    input()
    if insults == "yes":
        file.close()


main()
